<?php

define("BASE_PATH", "uploads/");

class ResizeImageConfig{
    
    private $path;
    private $height;
    private $width;
    
    public function setPath($path){
        $this->path = $path;   
    }
    public function getPath(){
        return $this->path;
    }
    
    public function setHeight($height){
        $this->height = $height;   
    }
    public function getHeight(){
        return $this->height;
    }
    
    public function setWidth($width){
        $this->width = $width;   
    }
    public function getWidth(){
        return $this->width;
    }
    
}

class FormRequests {

    private $file;
    private $uploadName;
    private $width;
    private $height;
    private $platforms;

    public function setFile($file) {
        $this->file = $file;
    }

    public function getFile() {
        return $this->file;
    }

    public function setUploadName($name) {
        $this->uploadName = trim($name);
    }

    public function getUploadName() {
        return $this->uploadName;
    }

    public function setWidth($width) {
        if($width){
            $this->width = $width;
        }else{
            $this->width = 0;
        }
    }

    public function getWidth() {
        return $this->width;
    }

    public function setHeight($height) {
        if($height){
            $this->height = $height;
        }else{
            $this->height = 0;
        }
    }

    public function getHeight() {
        return $this->height;
    }

    public function setPlatform($platform = array()) {
        $this->platforms = $platform;
    }

    public function getPlatform() {
        return $this->platforms;
    }

    /*
     * Check if the resize is to be done for iOS Platform
     */  
    public function isIOSChecked() {

        if ($this->platforms !== "iOS") {

            return false;
        }

        return true;
    }

    /*
     * Check if the resize is to be done for Android Platform
     */  
    public function isAndroidChecked() {

        if ($this->platforms !=="Android") {

            return false;
        }

        return true;
    }

    /*
     * Validate the form data
     */    
    public function validateEntry() {

        if (!((is_numeric($this->height) && ($this->height > 0)) || (is_numeric($this->width) && ($this->width > 0)))) {

            return false;
        } elseif (!(strlen($this->uploadName) > 0 && strpos($this->uploadName, '.') === false)) {

            return false;
        } elseif (!$this->file) {

            return false;
        }

        return true;
    }

    /*
     * Validate the uploade file is an image
     */
    public function validateImage() {
        
        if(!isset($this->file["tmp_name"])){
            
            return FALSE;
            
        }
        
        if(!$this->validateWithImgaeSize($this->file)){
            
            return FALSE;
            
        }else if(!$this->validateFileSize($this->file)){
            
            return FALSE;
            
        }else if (!$this->validateFileType($this->file)) {
            
            return FALSE;
            
        }
        
        return true;
        
    }
    
    /*
     * Validate the file size is Less than the passed size
     */
    private function validateFileSize( $size = 10000000){
        
        if(isset($this->file["size"]) && $this->file["size"] < $size){
            
            return true;
            
        }
        
        return false;
        
    }
    
    /*
     * Validate the file extention is either JPG OR PNG OR GIF
     */
    private function validateFileType(){
        
        $lowered_image_type = $this->getFileExtenstion();
        
        if ($lowered_image_type === "jpg" || $lowered_image_type === "png" || $lowered_image_type === "jpeg" && $lowered_image_type === "gif") {
            return true;
        }
        
        return true;
        
    }
    
    /*
     * Confirm the file is image by getting the image size
     */
    private function validateWithImgaeSize(){
        
       $imageSize = getimagesize($this->file["tmp_name"]);

        if ($imageSize === false) {
            
            return FALSE;
            
        }
        
        return TRUE;
        
    }
    
    /*
     * Get extention of the uploaded file
     */
    public function getFileExtenstion(){
        
        $imageFileType = pathinfo($this->file["name"],PATHINFO_EXTENSION);
        return strtolower($imageFileType);
        
    }
    
    /*
     * Get tmp path of the uploaded file
     */
    public function getFileTempPath(){
        
        return $this->file["tmp_name"];
        
    }

}

class AndroidImage{
    
    private $basePath;
    private $fileName;
    private $ext;
    private $baseHeight;
    private $baseWidth;
    
    public function __construct($basePath, $fileName, $ext, $baseHeight, $baseWidth) {
        
        $this->basePath     = $basePath;
        $this->fileName     = $fileName;
        $this->ext          = $ext;
        $this->baseHeight   = $baseHeight;
        $this->baseWidth    = $baseWidth;
        
    }
    
    public function getLDPIConfig(){
        
        return $this->getImageConfig("ldpi/", 3);
        
    }
    
    public function getMDPIConfig(){
        
        return $this->getImageConfig("mdpi/", 4);
        
    }
    
    public function getHDPIConfig(){

        return $this->getImageConfig("hdpi/", 6);

    }
    
    public function getXHDPIConfig(){

        return $this->getImageConfig("xhdpi/", 8);

    }
    
    public function getXXHDPIConfig(){

        return $this->getImageConfig("xxhdpi/", 12);

    }
    
    public function getXXXHDPIConfig(){

        return $this->getImageConfig("xxxhdpi/", 16);

    }
    
    private function getImageConfig($path, $multiplier){
        
        $obj = new ResizeImageConfig();
        $obj->setPath($this->basePath.$path.$this->fileName .".".$this->ext);
        $obj->setHeight(ceil($this->baseHeight*$multiplier));
        $obj->setWidth(ceil($this->baseWidth*$multiplier));
        
        return $obj;
        
    }
    
}

class iOSImage{
    
    private $basePath;
    private $fileName;
    private $ext;
    private $baseHeight;
    private $baseWidth;
    
    public function __construct($basePath, $fileName, $ext, $baseHeight, $baseWidth) {
        
        $this->basePath     = $basePath;
        $this->fileName     = $fileName;
        $this->ext          = $ext;
        $this->baseHeight   = $baseHeight;
        $this->baseWidth    = $baseWidth;
        
    }
    
    public function getNormalConfig(){
        
        return $this->getImageConfig(1);
        
    }
    
    public function getRetinaConfig(){
        
        return $this->getImageConfig(2,"@2x");
        
    }
    
    public function getHiResRetinaConfig(){

        return $this->getImageConfig(3,"@3x");

    }
    
    private function getImageConfig($multiplier, $identifier = ""){
        
        $obj = new ResizeImageConfig();
        $obj->setPath($this->basePath.$this->fileName.$identifier.".".$this->ext);
        $obj->setHeight(ceil($this->baseHeight*$multiplier));
        $obj->setWidth(ceil($this->baseWidth*$multiplier));
        
        return $obj;
        
    }
    
}

try {

    $request = new FormRequests();
    $request->setFile($_FILES["image"]);
    $request->setUploadName(filter_input(INPUT_POST, 'upload_name'));
    $request->setHeight(filter_input(INPUT_POST, 'height', FILTER_SANITIZE_NUMBER_FLOAT));
    $request->setWidth(filter_input(INPUT_POST, 'width', FILTER_SANITIZE_NUMBER_FLOAT));
    $request->setPlatform(filter_input(INPUT_POST, 'platform'));
} catch (Exception $e) {

    echo 'Exception With Message :' . $e->getMessage();
}

if (!$request->validateEntry()) {
    echo 'Invalid Data Entry';
    
}else if(!$request->validateImage()){
 
    echo 'Invalid Image';
    
}else{
    
    if ($request->isAndroidChecked()) {
    
        resizeImageForAndroid($request);

    }

    if($request->isIOSChecked()){

        resizeImageForiOS($request);

    }
    
}

function resizeImageForAndroid(FormRequests $request) {
    
    $ext = $request->getFileExtenstion();
    
    $name = $request->getUploadName();

    $baseWidth  = $request->getWidth()/4;
    $baseHeight = $request->getHeight()/4;
    
    $android_image_object = new AndroidImage(BASE_PATH, $name, $ext, $baseHeight, $baseWidth);
    
    echo "<br/>ldpi :" . create_scaled_image($android_image_object->getLDPIConfig(), $request->getFileTempPath());
    echo "<br/>mdpi :" . create_scaled_image($android_image_object->getMDPIConfig(), $request->getFileTempPath());
    echo "<br/>hdpi :" . create_scaled_image($android_image_object->getHDPIConfig(), $request->getFileTempPath());
    echo "<br/>xhdpi :" . create_scaled_image($android_image_object->getXHDPIConfig(), $request->getFileTempPath());
    echo "<br/>xxhdpi :" . create_scaled_image($android_image_object->getXXHDPIConfig(), $request->getFileTempPath());
    echo "<br/>xxxhdpi :" . create_scaled_image($android_image_object->getXXXHDPIConfig(), $request->getFileTempPath());
    
}

function resizeImageForiOS(FormRequests $request) {

    $ext = $request->getFileExtenstion();

    $directory = "ios/";
    $name = $request->getUploadName();

    $baseWidth = $request->getWidth();
    $baseHeight = $request->getHeight();
    
    $ios_image_object = new iOSImage(BASE_PATH.$directory , $name, $ext, $baseHeight, $baseWidth);
    
    echo "<br/>1x :" . create_scaled_image($ios_image_object->getNormalConfig(), $request->getFileTempPath());
    echo "<br/>2x :" . create_scaled_image($ios_image_object->getRetinaConfig(), $request->getFileTempPath());
    echo "<br/>3x :" . create_scaled_image($ios_image_object->getHiResRetinaConfig(), $request->getFileTempPath());

}

function create_scaled_image(ResizeImageConfig $image_config, $temp_file_name) {
    
    $new_file_path = $image_config->getPath();
    
    list($img_width, $img_height) = getimagesize($temp_file_name);

    if (!$img_width || !$img_height) {
        return false;
    }
    
    $scale = min(
        $image_config->getWidth() / $img_width, 
        $image_config->getHeight() / $img_height
    );
    
    if ($scale > 1) {
        $scale = 1;
    }  elseif($scale == 0) {
        
        $scale = max(
            $image_config->getWidth() / $img_width, 
            $image_config->getHeight() / $img_height
        );
        
    }
    
    $write_image = null;
    $new_width = $img_width * $scale;
    $new_height = $img_height * $scale;
    
    $new_img = imagecreatetruecolor($new_width, $new_height);

    switch (strtolower(substr(strrchr($image_config->getPath(), '.'), 1))) {
        case 'jpg':
        case 'jpeg':
            $src_img = imagecreatefromjpeg($temp_file_name);
            $write_image = 'imagejpeg';
            break;
        case 'gif':
            $src_img = imagecreatefromgif($temp_file_name);
            $write_image = 'imagegif';
            break;
        case 'png':
            $src_img = imagecreatefrompng($temp_file_name);
            $write_image = 'imagepng';

            break;
        default:
            $src_img = $image_method = null;
    }
    
    for( $x=0; $x<$img_width; $x++ ) {
        for( $y=0; $y<$img_height; $y++ ) {
     
            $alpha = ( imagecolorat( $src_img, $x, $y ) >> 24 & 0xFF );
        
        }
    
    }

    imagealphablending($new_img, false);
    imagesavealpha($new_img, true);
    $transparent = imagecolorallocatealpha($new_img, 255, 255, 255, 127);
    imagefilledrectangle($new_img, 0, 0, $new_width, $new_height, $transparent);

    $success = $src_img && 
            imagecopyresampled(
                    $new_img, 
                    $src_img, 
                    0, 
                    0, 
                    0, 
                    0, 
                    $new_width, 
                    $new_height, 
                    $img_width, 
                    $img_height
            ) && 
            $write_image($new_img, $new_file_path);

    // Free up memory (imagedestroy does not delete files):
    imagedestroy($src_img);
    imagedestroy($new_img);
    return ($success) ? "true" : "false";
}



//function colorizeBasedOnAplhaChannnel( $file, $targetR, $targetG, $targetB, $targetName ) {
//
//    $im_src = imagecreatefrompng( $file );
//
//    $width = imagesx($im_src);
//    $height = imagesy($im_src);
//
//    $im_dst = imagecreatefrompng( $file );
//
//    // Note this:
//    // Let's reduce the number of colors in the image to ONE
//    imagefilledrectangle( $im_dst, 0, 0, $width, $height, 0xFFFFFF );
//
//    for( $x=0; $x<$width; $x++ ) {
//        for( $y=0; $y<$height; $y++ ) {
//
//            $alpha = ( imagecolorat( $im_src, $x, $y ) >> 24 & 0xFF );
//
//            $col = imagecolorallocatealpha( $im_dst,
//                $targetR - (int) ( 1.0 / 255.0  * $alpha * (double) $targetR ),
//                $targetG - (int) ( 1.0 / 255.0  * $alpha * (double) $targetG ),
//                $targetB - (int) ( 1.0 / 255.0  * $alpha * (double) $targetB ),
//                $alpha
//                );
//
//            if ( false === $col ) {
//                die( 'sorry, out of colors...' );
//            }
//
//            imagesetpixel( $im_dst, $x, $y, $col );
//
//        }
//
//    }
//
//    imagepng( $im_dst, $targetName);
//    imagedestroy($im_dst);
//
//}
//
//unlink( dirname ( __FILE__ ) . '/newleaf.png' );
//unlink( dirname ( __FILE__ ) . '/newleaf1.png' );
//unlink( dirname ( __FILE__ ) . '/newleaf2.png' );
//
//$img = dirname ( __FILE__ ) . '/leaf.png';
//colorizeBasedOnAplhaChannnel( $img, 0, 0, 0xFF, 'newleaf1.png' );
//colorizeBasedOnAplhaChannnel( $img, 0xFF, 0, 0xFF, 'newleaf2.png' );
//
?>