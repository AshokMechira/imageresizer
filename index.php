<!DOCTYPE html>
<html>
    <head>
        <script>
            function handleClick(myRadio) {
                
                var x = document.getElementsByName("imageSizeName");
                
                var i;
                for (i = 0; i < x.length; i++) {
                    if(myRadio.value == "iOS"){

                        x[i].innerHTML = "1x";

                    }else{

                        x[i].innerHTML = "mdpi";

                    }
                }
            }
        </script>
        <style>
            html, body {
                height: 100%;
                background-color: #AAAAAA;
                
            }
            .container {
                margin: auto;
                width : 435px;
                vertical-align: middle;
                background-color: #CACACA;
                position: relative;
                top: 45%;
                transform: translateY(-50%);
                border-radius: 25px;
                padding: 20px; 
            }
            .form-title{
                text-align: center;
                color: #343434;
                
            }
            label{
                display: block;
                margin-bottom: 20px;
            }
            
            .submit-button{
                text-align: center;
                background-color: #ffff00;
                margin: 0 auto;
                width: 100px;
                height : 35px;
                
            }
            
            .field-title{
                
                text-align: right;
            }
            
            .notifier{
                font-size: 12px;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <h1 class="form-title">Image Resizer</h1>
            <form class="resizeform" name="resizerform" action="upload.php" method="post" enctype="multipart/form-data">
                <table cellspacing="10px">
                    <tr>
                        <td class="field-title">
                            Select Platform : 
                        </td>
                        <td>
                            <input type="radio" name="platform" value="iOS" checked="checked" onclick="handleClick(this);">iOS
                            <input type="radio" name="platform" value="Android" onclick="handleClick(this);"> Android
                        </td>
                    </tr>
                    <tr>
                        <td class="field-title">
                            Select image to be resized :
                        </td>
                        <td>
                            <input type="file" name="image" id="fileToUpload">
                        </td>
                    </tr>
                    <tr>
                        <td class="field-title">
                            Required image name :
                        </td>
                        <td>
                            <input type="text" name="upload_name" id="file_name">
                        </td>
                    </tr>
                    <tr>
                        <td class="field-title">
                            Required <span name="imageSizeName">1x</span> width :
                        </td>
                        <td>
                            <input type="text" name="width" id="width">
                        </td>
                    </tr>
                    <tr>
                        <td class="field-title">
                            Required <span name="imageSizeName">1x</span> height :
                        </td>
                        <td>
                            <input type="text" name="height" id="height">
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input class="submit-button" type="submit" value="Resize Image" name="submit">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="notifier">
                                * Image sizes are calculate with aspect ratio. If a single size parameter is provided, the image will be resized based on the provided parameter and the aspect ratio.
                            </span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
